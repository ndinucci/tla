object Jugador1{
	int decidirJugada(){
		int jugada;
		jugada = 0;
		printf("Turno del jugador 1 ");
		while(jugada != 1 && jugada != 2 && jugada !=3){
			jugada = getint("Ingrese su jugada ( 1 - Piedra    2- Papel  3 - Tijera) ");
		}
		return jugada;
	}
}

object Jugador2{
	int decidirJugada(){
		int jugada;
		jugada = 0;
		printf("Turno del jugador 2 ");
		while(jugada != 1 && jugada != 2 && jugada !=3){
			jugada = getint("Ingrese su jugada ( 1 - Piedra    2- Papel  3 - Tijera) ");
		}
		return jugada;
	}
}

object Juego{
	int jugarPiedraPapelOTijera(){
		int jugada1;
		int jugada2;
		jugada1=Jugador1.decidirJugada();
		jugada2=Jugador2.decidirJugada();
		if(jugada1 == jugada2){
			return 3;
		}
		switch(jugada1){
			case 1:{
				if(jugada2 == 2){
					return 2;
				}
				if(jugada2 == 3){
					return 1;
				}
				break;
			}
			case 2:{
				if(jugada2 == 1){
					return 1;
				}
				if(jugada2 == 3){
					return 2;
				}
				break;
			}
			case 3:{
				if(jugada2 == 1){
					return 2;
				} 
				if(jugada2 == 2){
					return 1;
				}
				break;
			}
			default:{
				break;
			}
		}
		return 0;
	}	
}

int main(void){
	int resultado;
	resultado = Juego.jugarPiedraPapelOTijera();
	switch(resultado){
		case 1:{
			printf("Gano el jugador 1!");
			break;
		}
		case 2:{
			printf("Gano el jugador 2!");
			break;
		}
		case 3:{
			printf("Han empatado!");
			break;
		}
		default:{
			break;
		}
	}
	return 0;
}