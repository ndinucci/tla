object Juego{
	
	int oportunidades;
	int numero;

	int jugar(){
		oportunidades = 5;
		numero = 42;
		int intento;
		printf("Bienvenido al juego, tienes 5 oportunidades para adivinar el numero magico.\n");
		while(oportunidades > 0){
			intento = getint("Elige un numero:");
			if(intento == numero){
				return 1;
			}
			if( intento > numero){
				printf("El numero magico es menor al elegido\n");
			} else {
				printf("El numero magico es mayor al elegido\n");
			}
			oportunidades = oportunidades - 1;
			printf("Te quedan %d oportunidades\n", oportunidades);
		}
		return 0;
	}
}

int main (void) {
	int resultado;
	int eleccion;
	resultado = 0;
	eleccion = 0;
	while(eleccion == 0 && resultado == 0){
		resultado = Juego.jugar();
		if(resultado == 0){
			printf("Perdiste! \n");
			eleccion = 2;
			while(eleccion != 0 && eleccion != 1){
				eleccion = getint("0- Intentar de nuevo \n1- Salir");
			}
		} else {
			printf("Ganaste! \n");
		}
	}
	return 0;
}