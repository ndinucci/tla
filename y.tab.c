/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     FOR_TOKEN = 258,
     WHILE_TOKEN = 259,
     IF_TOKEN = 260,
     ELSE_TOKEN = 261,
     SWITCH_TOKEN = 262,
     DO_TOKEN = 263,
     OP_PLUS = 264,
     OP_MINUS = 265,
     OP_MULT = 266,
     OP_DIV = 267,
     OP_MOD = 268,
     OP_ASSIGN = 269,
     OP_GT = 270,
     OP_LT = 271,
     OP_GE = 272,
     OP_LE = 273,
     OP_EQ = 274,
     OP_AND = 275,
     OP_OR = 276,
     NOT = 277,
     OP_NE = 278,
     OBJECT_TOKEN = 279,
     TRUE_TOKEN = 280,
     FALSE_TOKEN = 281,
     NULL_TOKEN = 282,
     BREAK_TOKEN = 283,
     OPEN_C_BRACKET = 284,
     CLOSE_C_BRACKET = 285,
     OPEN_S_BRACKET = 286,
     CLOSE_S_BRACKET = 287,
     OPEN_PARENTHESIS = 288,
     CLOSE_PARENTHESIS = 289,
     COLON = 290,
     SEMICOLON = 291,
     COMMA = 292,
     DOT = 293,
     PRINT = 294,
     MAIN = 295,
     VOID = 296,
     CASE = 297,
     DEFAULT = 298,
     END = 299,
     RETURN_TOKEN = 300,
     NAME = 301,
     STRING = 302,
     NUMBER = 303,
     TYPE = 304
   };
#endif
/* Tokens.  */
#define FOR_TOKEN 258
#define WHILE_TOKEN 259
#define IF_TOKEN 260
#define ELSE_TOKEN 261
#define SWITCH_TOKEN 262
#define DO_TOKEN 263
#define OP_PLUS 264
#define OP_MINUS 265
#define OP_MULT 266
#define OP_DIV 267
#define OP_MOD 268
#define OP_ASSIGN 269
#define OP_GT 270
#define OP_LT 271
#define OP_GE 272
#define OP_LE 273
#define OP_EQ 274
#define OP_AND 275
#define OP_OR 276
#define NOT 277
#define OP_NE 278
#define OBJECT_TOKEN 279
#define TRUE_TOKEN 280
#define FALSE_TOKEN 281
#define NULL_TOKEN 282
#define BREAK_TOKEN 283
#define OPEN_C_BRACKET 284
#define CLOSE_C_BRACKET 285
#define OPEN_S_BRACKET 286
#define CLOSE_S_BRACKET 287
#define OPEN_PARENTHESIS 288
#define CLOSE_PARENTHESIS 289
#define COLON 290
#define SEMICOLON 291
#define COMMA 292
#define DOT 293
#define PRINT 294
#define MAIN 295
#define VOID 296
#define CASE 297
#define DEFAULT 298
#define END 299
#define RETURN_TOKEN 300
#define NAME 301
#define STRING 302
#define NUMBER 303
#define TYPE 304




/* Copy the first part of user declarations.  */
#line 1 "roseta.y"


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>

char* concat_str(int argc, ...);


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 54 "roseta.y"
{
	char* strval;
}
/* Line 193 of yacc.c.  */
#line 209 "y.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 222 "y.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  6
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   189

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  50
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  26
/* YYNRULES -- Number of rules.  */
#define YYNRULES  71
/* YYNRULES -- Number of states.  */
#define YYNSTATES  176

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   304

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     6,     9,    11,    18,    24,    28,    31,
      34,    37,    39,    44,    49,    53,    56,    57,    61,    62,
      66,    70,    74,    80,    86,    93,   104,   111,   120,   129,
     138,   142,   145,   146,   148,   152,   153,   155,   157,   161,
     165,   169,   171,   175,   179,   181,   185,   189,   193,   197,
     199,   203,   207,   209,   213,   215,   219,   222,   223,   227,
     228,   230,   232,   234,   236,   238,   240,   247,   252,   256,
     262,   266
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      51,     0,    -1,    52,    75,    -1,    52,    53,    -1,    53,
      -1,    24,    46,    29,    54,    56,    30,    -1,    24,    46,
      29,    56,    30,    -1,    54,    55,    36,    -1,    55,    36,
      -1,    49,    46,    -1,    56,    57,    -1,    57,    -1,    49,
      46,    58,    61,    -1,    41,    46,    58,    61,    -1,    33,
      59,    34,    -1,    55,    60,    -1,    -1,    60,    37,    55,
      -1,    -1,    29,    62,    30,    -1,    55,    36,    62,    -1,
      64,    36,    62,    -1,    55,    14,    64,    36,    62,    -1,
      46,    14,    64,    36,    62,    -1,     4,    33,    64,    34,
      61,    62,    -1,     3,    33,    63,    36,    64,    36,    63,
      34,    61,    62,    -1,     5,    33,    64,    34,    61,    62,
      -1,     5,    33,    64,    34,    61,     6,    61,    62,    -1,
       7,    33,    64,    34,    29,    74,    30,    62,    -1,     8,
      61,     4,    33,    64,    34,    36,    62,    -1,    45,    64,
      36,    -1,    28,    36,    -1,    -1,    64,    -1,    46,    14,
      64,    -1,    -1,    70,    -1,    73,    -1,    65,    11,    73,
      -1,    65,    12,    73,    -1,    65,    13,    73,    -1,    65,
      -1,    66,     9,    65,    -1,    66,    10,    65,    -1,    66,
      -1,    67,    16,    66,    -1,    67,    15,    66,    -1,    67,
      18,    66,    -1,    67,    17,    66,    -1,    67,    -1,    68,
      19,    67,    -1,    68,    23,    67,    -1,    68,    -1,    69,
      20,    68,    -1,    69,    -1,    70,    21,    69,    -1,    64,
      72,    -1,    -1,    72,    37,    64,    -1,    -1,    46,    -1,
      48,    -1,    25,    -1,    26,    -1,    47,    -1,    27,    -1,
      46,    38,    46,    33,    71,    34,    -1,    46,    33,    71,
      34,    -1,    33,    64,    34,    -1,    42,    64,    35,    61,
      74,    -1,    43,    35,    61,    -1,    49,    40,    33,    41,
      34,    61,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    63,    63,    68,    70,    75,    77,    82,    84,    89,
      94,    96,   101,   103,   109,   114,   117,   121,   124,   128,
     133,   135,   137,   139,   141,   143,   145,   147,   149,   151,
     153,   155,   158,   162,   164,   167,   171,   176,   178,   180,
     182,   187,   189,   191,   196,   198,   200,   202,   204,   209,
     211,   213,   218,   220,   225,   227,   232,   235,   239,   242,
     246,   248,   250,   252,   254,   256,   258,   260,   262,   267,
     269,   274
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "FOR_TOKEN", "WHILE_TOKEN", "IF_TOKEN",
  "ELSE_TOKEN", "SWITCH_TOKEN", "DO_TOKEN", "OP_PLUS", "OP_MINUS",
  "OP_MULT", "OP_DIV", "OP_MOD", "OP_ASSIGN", "OP_GT", "OP_LT", "OP_GE",
  "OP_LE", "OP_EQ", "OP_AND", "OP_OR", "NOT", "OP_NE", "OBJECT_TOKEN",
  "TRUE_TOKEN", "FALSE_TOKEN", "NULL_TOKEN", "BREAK_TOKEN",
  "OPEN_C_BRACKET", "CLOSE_C_BRACKET", "OPEN_S_BRACKET", "CLOSE_S_BRACKET",
  "OPEN_PARENTHESIS", "CLOSE_PARENTHESIS", "COLON", "SEMICOLON", "COMMA",
  "DOT", "PRINT", "MAIN", "VOID", "CASE", "DEFAULT", "END", "RETURN_TOKEN",
  "NAME", "STRING", "NUMBER", "TYPE", "$accept", "Program", "Objects",
  "Class", "Variables", "Variable", "Functions", "Function", "Parameters",
  "Variable_1", "Variable_2", "Bloque", "Statement", "ForExp",
  "Expression", "MultiplicativeExpression", "AdditiveExpression",
  "RelationalExpression", "EqualityExpression", "ConditionalAndExpression",
  "ConditionalOrExpression", "Value_1", "Value_2", "Term", "Cases", "Main", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    50,    51,    52,    52,    53,    53,    54,    54,    55,
      56,    56,    57,    57,    58,    59,    59,    60,    60,    61,
      62,    62,    62,    62,    62,    62,    62,    62,    62,    62,
      62,    62,    62,    63,    63,    63,    64,    65,    65,    65,
      65,    66,    66,    66,    67,    67,    67,    67,    67,    68,
      68,    68,    69,    69,    70,    70,    71,    71,    72,    72,
      73,    73,    73,    73,    73,    73,    73,    73,    73,    74,
      74,    75
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     2,     1,     6,     5,     3,     2,     2,
       2,     1,     4,     4,     3,     2,     0,     3,     0,     3,
       3,     3,     5,     5,     6,    10,     6,     8,     8,     8,
       3,     2,     0,     1,     3,     0,     1,     1,     3,     3,
       3,     1,     3,     3,     1,     3,     3,     3,     3,     1,
       3,     3,     1,     3,     1,     3,     2,     0,     3,     0,
       1,     1,     1,     1,     1,     1,     6,     4,     3,     5,
       3,     6
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,     4,     0,     1,     0,     3,     2,
       0,     0,     0,     0,     0,     0,     0,    11,     0,     0,
       9,     0,     0,     8,     6,     0,    10,     0,    16,     0,
       0,     7,     5,     0,     0,     0,    18,     0,    32,    13,
      12,    71,     9,    15,    14,     0,     0,     0,     0,     0,
      62,    63,    65,     0,     0,     0,    60,    64,    61,     0,
       0,     0,    41,    44,    49,    52,    54,    36,    37,     0,
      35,     0,     0,     0,     0,    31,    60,     0,     0,     0,
      57,     0,     0,    32,    19,    32,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    17,
      60,     0,    33,     0,     0,     0,     0,    68,    30,     0,
      59,     0,     0,     0,    20,    21,    38,    39,    40,    42,
      43,    46,    45,    48,    47,    50,    51,    53,    55,     0,
       0,     0,     0,     0,     0,    32,    56,    67,    57,    32,
      34,     0,    32,    32,     0,     0,    23,     0,     0,    22,
      35,    24,     0,    26,     0,     0,     0,     0,    58,    66,
       0,    32,     0,     0,    32,    32,     0,    27,     0,    70,
      28,    29,    32,     0,    25,    69
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,     4,    14,    59,    16,    17,    30,    37,
      43,    39,    60,   101,    61,    62,    63,    64,    65,    66,
      67,   111,   136,    68,   156,     9
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -81
static const yytype_int16 yypact[] =
{
     -23,    12,    53,   -15,   -81,    31,   -81,    40,   -81,   -81,
       0,    79,    67,    68,     0,    50,   -20,   -81,    74,    88,
      88,    90,   -19,   -81,   -81,    83,   -81,    96,    82,   103,
     103,   -81,   -81,    88,   103,    89,   -81,   100,   120,   -81,
     -81,   -81,   -81,   102,   -81,   104,   107,   108,   109,   103,
     -81,   -81,   -81,   113,    18,    18,    -2,   -81,   -81,     2,
     114,   115,    77,     5,    55,    -6,   123,   129,   -81,    82,
      21,    18,    18,    18,   148,   -81,    19,   121,   118,    18,
      18,   110,    18,   120,   -81,   120,    18,    18,    18,    18,
      18,    18,    18,    18,    18,    18,    18,    18,    18,   -81,
       4,   122,   -81,   125,   126,   127,   124,   -81,   -81,   128,
     -81,   136,   130,   135,   -81,   -81,   -81,   -81,   -81,    77,
      77,     5,     5,     5,     5,    55,    55,    -6,   123,    18,
      18,   103,   103,   133,    18,   120,   137,   -81,    18,   120,
     -81,   139,   120,    71,    -3,   138,   -81,    18,   142,   -81,
      21,   -81,   103,   -81,    18,   143,   147,   144,   -81,   -81,
     145,   120,   146,   103,   120,   120,   103,   -81,   103,   -81,
     -81,   -81,   120,    -3,   -81,   -81
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -81,   -81,   -81,   170,   -81,    -8,   168,    34,   164,   -81,
     -81,   -30,   -80,    35,   -47,   -62,    17,    10,    87,    91,
     -81,    48,   -81,     7,    14,   -81
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      40,     1,    15,   114,    41,   115,    21,    77,    78,     1,
      24,    32,    79,    95,    89,    90,    82,    96,   129,    74,
      36,    12,    12,   102,   103,   104,   105,   119,   120,    25,
      25,    80,   109,   110,     7,   113,    81,    80,    83,   154,
     155,    12,    81,    50,    51,    52,    50,    51,    52,    13,
      26,    54,    80,     6,    54,   146,    26,    81,     5,   149,
      10,    99,   151,   153,    76,    57,    58,   100,    57,    58,
      91,    92,    93,    94,    45,    46,    47,   152,    48,    49,
      11,   167,   140,   141,   170,   171,    23,   145,    86,    87,
      88,   110,   174,   116,   117,   118,    50,    51,    52,    53,
     158,   142,   143,   102,    54,   125,   126,   162,   121,   122,
     123,   124,    18,    19,    20,    27,    55,    56,    57,    58,
      35,    28,   161,    45,    46,    47,    31,    48,    49,    33,
      34,    35,    38,   169,    44,    42,   172,    70,   173,    69,
      71,    72,    73,    97,    84,    50,    51,    52,    53,    75,
      98,    85,   106,    54,   108,   107,   112,   134,   130,   131,
     132,   133,   144,   138,   135,    55,    56,    57,    58,    35,
     137,   139,   157,     8,   147,   150,   159,   164,   163,   166,
     165,   168,    22,    29,   127,   160,   148,   175,     0,   128
};

static const yytype_int16 yycheck[] =
{
      30,    24,    10,    83,    34,    85,    14,    54,    55,    24,
      30,    30,    14,    19,     9,    10,    14,    23,    14,    49,
      28,    41,    41,    70,    71,    72,    73,    89,    90,    49,
      49,    33,    79,    80,    49,    82,    38,    33,    36,    42,
      43,    41,    38,    25,    26,    27,    25,    26,    27,    49,
      16,    33,    33,     0,    33,   135,    22,    38,    46,   139,
      29,    69,   142,   143,    46,    47,    48,    46,    47,    48,
      15,    16,    17,    18,     3,     4,     5,     6,     7,     8,
      40,   161,   129,   130,   164,   165,    36,   134,    11,    12,
      13,   138,   172,    86,    87,    88,    25,    26,    27,    28,
     147,   131,   132,   150,    33,    95,    96,   154,    91,    92,
      93,    94,    33,    46,    46,    41,    45,    46,    47,    48,
      49,    33,   152,     3,     4,     5,    36,     7,     8,    46,
      34,    49,    29,   163,    34,    46,   166,    33,   168,    37,
      33,    33,    33,    20,    30,    25,    26,    27,    28,    36,
      21,    36,     4,    33,    36,    34,    46,    33,    36,    34,
      34,    34,    29,    33,    36,    45,    46,    47,    48,    49,
      34,    36,    34,     3,    37,    36,    34,    30,    35,    34,
      36,    35,    14,    19,    97,   150,   138,   173,    -1,    98
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    24,    51,    52,    53,    46,     0,    49,    53,    75,
      29,    40,    41,    49,    54,    55,    56,    57,    33,    46,
      46,    55,    56,    36,    30,    49,    57,    41,    33,    58,
      58,    36,    30,    46,    34,    49,    55,    59,    29,    61,
      61,    61,    46,    60,    34,     3,     4,     5,     7,     8,
      25,    26,    27,    28,    33,    45,    46,    47,    48,    55,
      62,    64,    65,    66,    67,    68,    69,    70,    73,    37,
      33,    33,    33,    33,    61,    36,    46,    64,    64,    14,
      33,    38,    14,    36,    30,    36,    11,    12,    13,     9,
      10,    15,    16,    17,    18,    19,    23,    20,    21,    55,
      46,    63,    64,    64,    64,    64,     4,    34,    36,    64,
      64,    71,    46,    64,    62,    62,    73,    73,    73,    65,
      65,    66,    66,    66,    66,    67,    67,    68,    69,    14,
      36,    34,    34,    34,    33,    36,    72,    34,    33,    36,
      64,    64,    61,    61,    29,    64,    62,    37,    71,    62,
      36,    62,     6,    62,    42,    43,    74,    34,    64,    34,
      63,    61,    64,    35,    30,    36,    34,    62,    35,    61,
      62,    62,    61,    61,    62,    74
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 64 "roseta.y"
    { printf("%s%s%s\n", (yyvsp[(1) - (2)].strval), "\'", (yyvsp[(2) - (2)].strval)); }
    break;

  case 3:
#line 69 "roseta.y"
    { (yyval.strval) = concat_str( 2, (yyvsp[(1) - (2)].strval), (yyvsp[(2) - (2)].strval)); }
    break;

  case 4:
#line 71 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 5:
#line 76 "roseta.y"
    { (yyval.strval) = concat_str(5, (yyvsp[(2) - (6)].strval), "\n", (yyvsp[(4) - (6)].strval), (yyvsp[(5) - (6)].strval), "?"); }
    break;

  case 6:
#line 78 "roseta.y"
    { (yyval.strval) = concat_str(4, (yyvsp[(2) - (5)].strval), "\n", (yyvsp[(4) - (5)].strval), "?"); }
    break;

  case 7:
#line 83 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), (yyvsp[(2) - (3)].strval), ";\n"); }
    break;

  case 8:
#line 85 "roseta.y"
    { (yyval.strval) = concat_str(2, (yyvsp[(1) - (2)].strval), ";\n"); }
    break;

  case 9:
#line 90 "roseta.y"
    { (yyval.strval) = concat_str( 3, (yyvsp[(1) - (2)].strval), " ~_", (yyvsp[(2) - (2)].strval)); }
    break;

  case 10:
#line 95 "roseta.y"
    { (yyval.strval) = concat_str( 3, (yyvsp[(1) - (2)].strval), "\n", (yyvsp[(2) - (2)].strval)); }
    break;

  case 11:
#line 97 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 12:
#line 102 "roseta.y"
    { (yyval.strval) = concat_str(7, (yyvsp[(1) - (4)].strval), " ~_", (yyvsp[(2) - (4)].strval), (yyvsp[(3) - (4)].strval), "\n", (yyvsp[(4) - (4)].strval), "\n"); }
    break;

  case 13:
#line 104 "roseta.y"
    { (yyval.strval) = concat_str(7, "void "," ~_", (yyvsp[(2) - (4)].strval), (yyvsp[(3) - (4)].strval), "\n", (yyvsp[(4) - (4)].strval), "\n"); }
    break;

  case 14:
#line 110 "roseta.y"
    { (yyval.strval) = concat_str( 3, "( ", (yyvsp[(2) - (3)].strval), " )"); }
    break;

  case 15:
#line 115 "roseta.y"
    { (yyval.strval) = concat_str( 2, (yyvsp[(1) - (2)].strval), (yyvsp[(2) - (2)].strval)); }
    break;

  case 16:
#line 117 "roseta.y"
    { (yyval.strval) = ""; }
    break;

  case 17:
#line 122 "roseta.y"
    { (yyval.strval) = concat_str( 3, (yyvsp[(1) - (3)].strval),", ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 18:
#line 124 "roseta.y"
    { (yyval.strval) = ""; }
    break;

  case 19:
#line 129 "roseta.y"
    { (yyval.strval) = concat_str( 3, "{\n", (yyvsp[(2) - (3)].strval), "\n}"); }
    break;

  case 20:
#line 134 "roseta.y"
    { (yyval.strval) = concat_str( 3, (yyvsp[(1) - (3)].strval), ";\n",  (yyvsp[(3) - (3)].strval)); }
    break;

  case 21:
#line 136 "roseta.y"
    { (yyval.strval) = concat_str( 3, (yyvsp[(1) - (3)].strval), ";\n", (yyvsp[(3) - (3)].strval)); }
    break;

  case 22:
#line 138 "roseta.y"
    { (yyval.strval) = concat_str( 5, (yyvsp[(1) - (5)].strval), " = ", (yyvsp[(3) - (5)].strval), ";\n", (yyvsp[(5) - (5)].strval)); }
    break;

  case 23:
#line 140 "roseta.y"
    { (yyval.strval) = concat_str( 6, "~_", (yyvsp[(1) - (5)].strval), " = ", (yyvsp[(3) - (5)].strval), ";\n", (yyvsp[(5) - (5)].strval)); }
    break;

  case 24:
#line 142 "roseta.y"
    { (yyval.strval) = concat_str( 6,"while ( ", (yyvsp[(3) - (6)].strval), " )\n", (yyvsp[(5) - (6)].strval), "\n", (yyvsp[(6) - (6)].strval)); }
    break;

  case 25:
#line 144 "roseta.y"
    { (yyval.strval) = concat_str( 10, "for ( ", (yyvsp[(3) - (10)].strval), " ; ", (yyvsp[(5) - (10)].strval), " ; ", (yyvsp[(7) - (10)].strval), " )\n", (yyvsp[(9) - (10)].strval), "\n", (yyvsp[(10) - (10)].strval)); }
    break;

  case 26:
#line 146 "roseta.y"
    { (yyval.strval) = concat_str( 6, "if ( ", (yyvsp[(3) - (6)].strval), " )\n", (yyvsp[(5) - (6)].strval), "\n", (yyvsp[(6) - (6)].strval)) ; }
    break;

  case 27:
#line 148 "roseta.y"
    { (yyval.strval) = concat_str( 9, "if ( ",  (yyvsp[(3) - (8)].strval), " )\n", (yyvsp[(5) - (8)].strval), "\n", "else\n", (yyvsp[(7) - (8)].strval) , "\n", (yyvsp[(8) - (8)].strval)); }
    break;

  case 28:
#line 150 "roseta.y"
    { (yyval.strval) = concat_str( 6, "switch ( ", (yyvsp[(3) - (8)].strval), " )\n{\n", (yyvsp[(6) - (8)].strval), "\n}", (yyvsp[(8) - (8)].strval)); }
    break;

  case 29:
#line 152 "roseta.y"
    { (yyval.strval) = concat_str( 7, "do\n", (yyvsp[(2) - (8)].strval), "\n", "while ( ", (yyvsp[(5) - (8)].strval), " );\n", (yyvsp[(8) - (8)].strval)); }
    break;

  case 30:
#line 154 "roseta.y"
    { (yyval.strval) = concat_str( 3, "return ", (yyvsp[(2) - (3)].strval), ";");}
    break;

  case 31:
#line 156 "roseta.y"
    { (yyval.strval) = "break;\n"; }
    break;

  case 32:
#line 158 "roseta.y"
    { (yyval.strval) = ""; }
    break;

  case 33:
#line 163 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 34:
#line 165 "roseta.y"
    { (yyval.strval) = concat_str( 4, "~_", (yyvsp[(1) - (3)].strval), " = ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 35:
#line 167 "roseta.y"
    { (yyval.strval) = ""; }
    break;

  case 36:
#line 172 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 37:
#line 177 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 38:
#line 179 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " * ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 39:
#line 181 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " / ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 40:
#line 183 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " % ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 41:
#line 188 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 42:
#line 190 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " + ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 43:
#line 192 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " - ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 44:
#line 197 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 45:
#line 199 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " < ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 46:
#line 201 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " > ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 47:
#line 203 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " <= ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 48:
#line 205 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " >= ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 49:
#line 210 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 50:
#line 212 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " == ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 51:
#line 214 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " != ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 52:
#line 219 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 53:
#line 221 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " && ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 54:
#line 226 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 55:
#line 228 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (3)].strval), " || ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 56:
#line 233 "roseta.y"
    { (yyval.strval) = concat_str( 2, (yyvsp[(1) - (2)].strval), (yyvsp[(2) - (2)].strval)); }
    break;

  case 57:
#line 235 "roseta.y"
    { (yyval.strval) = ""; }
    break;

  case 58:
#line 240 "roseta.y"
    { (yyval.strval) = concat_str( 3, (yyvsp[(1) - (3)].strval), ", ", (yyvsp[(3) - (3)].strval)); }
    break;

  case 59:
#line 242 "roseta.y"
    { (yyval.strval) = ""; }
    break;

  case 60:
#line 247 "roseta.y"
    { (yyval.strval) = concat_str(2, "~_", (yyvsp[(1) - (1)].strval)); }
    break;

  case 61:
#line 249 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 62:
#line 251 "roseta.y"
    { (yyval.strval) = "true"; }
    break;

  case 63:
#line 253 "roseta.y"
    { (yyval.strval) = "false"; }
    break;

  case 64:
#line 255 "roseta.y"
    { (yyval.strval) = (yyvsp[(1) - (1)].strval); }
    break;

  case 65:
#line 257 "roseta.y"
    { (yyval.strval) = "null"; }
    break;

  case 66:
#line 259 "roseta.y"
    { (yyval.strval) = concat_str( 6, (yyvsp[(1) - (6)].strval), "_", (yyvsp[(3) - (6)].strval), "( ", (yyvsp[(5) - (6)].strval), " )"); }
    break;

  case 67:
#line 261 "roseta.y"
    { (yyval.strval) = concat_str( 4, (yyvsp[(1) - (4)].strval), "( ", (yyvsp[(3) - (4)].strval), " )"); }
    break;

  case 68:
#line 263 "roseta.y"
    { (yyval.strval) = concat_str( 3, "( ", (yyvsp[(2) - (3)].strval), " )"); }
    break;

  case 69:
#line 268 "roseta.y"
    { (yyval.strval) = concat_str(5, "case ", (yyvsp[(2) - (5)].strval), " : ", (yyvsp[(4) - (5)].strval), (yyvsp[(5) - (5)].strval)); }
    break;

  case 70:
#line 270 "roseta.y"
    { (yyval.strval) = concat_str(2, "default : \n", (yyvsp[(3) - (3)].strval)); }
    break;

  case 71:
#line 275 "roseta.y"
    { (yyval.strval) = concat_str(3, (yyvsp[(1) - (6)].strval), " main ()\n",(yyvsp[(6) - (6)].strval)); }
    break;


/* Line 1267 of yacc.c.  */
#line 1918 "y.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 279 "roseta.y"


char* 
concat_str(int argc, ...){
	
   char * ans = NULL;
   char ** args = (char **)malloc(argc*sizeof(char *));

   int size = 0, i;

   va_list ap;
   va_start(ap, argc);
   
   for(i = 0; i < argc; i++)
   {
      args[i] = va_arg(ap, char *);
      size += strlen(args[i]);
   }

   ans = (char *)malloc((size+1)*sizeof(char)); // size+1 para el '\0'

   for(i = 0; i < argc; i++)
      sprintf(ans, "%s%s", ans, args[i]);

   va_end(ap);
   return ans;
}

int 
main() {
	printf("");
	yyparse();
}













