all:
	yacc -d roseta.y
	lex roseta.l
	gcc -o parser lex.yy.c y.tab.c -ly
	gcc -o traduction traduction.c
	
clean:
	rm -f aux.c
	rm -f parser
	rm -f traduction
	rm -f lex.yy.c
	rm -f y.tab.c
	rm -f y.tab.h
	rm -f output.c