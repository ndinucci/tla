# Roseta #

En esta carpeta se incluye el parser para pasar de Roseta, un lenguaje orientado a objetos, a un lenguaje imperativo C. Además se incluyen 10 archivos .rs con sus respectivos ejecutables y el informe del trabajo en .pdf. 

##Requerimientos##

Para poder correr este proyecto se debe tener instalado yacc y lex. 

##Instrucciones de uso##
* **make**  (Genera el parser a partir de los archivos roseta.y y roseta.l)
* **./compile.sh myprogram** (Busca myprogram.rs y genera el ejecutable bajo el nombre de myprogram)

Para borrar el parser y sus archivos auxiliares ejecutar  **make clean**


###Integrantes del grupo###

* Di Nucci, Nicolás Santiago
* Rossi, Melisa Anabella
* Zannini, Franco Michel