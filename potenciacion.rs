object Math{

	int pow(int base,int exponente){
		int i;
		int respuesta;
		respuesta = 1;
		for(i = 0; i < exponente ; i = i + 1){
			respuesta = respuesta * base;
		}
		return respuesta;
	}
}


int main(void){
	int base;
	int exponente;
	int resultado;
	resultado = 0;
	base = -1;
	exponente = -1;
	while(base <= 0){
		base = getint("Ingrese un numero entero positivo como base: ");
	}
	while(exponente <= 0){
		exponente = getint("Ingrese un numero entero mayor o igual a 0 como exponente: ");
	}
	resultado = Math.pow(base,exponente);
	printf("El resultado de elevar %d a la %d es %d .", base, exponente, resultado);
	return 0;
}