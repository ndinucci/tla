%{
#include "y.tab.h"

%}
%option nounput yylineno

%%

[\"].*[\"]						{yylval.strval=strdup(yytext);return STRING;}
"for"							{return FOR_TOKEN;}
"while"							{return WHILE_TOKEN;}
"if"							{return IF_TOKEN;}
"do"							{return DO_TOKEN;}
"else"							{return ELSE_TOKEN;}
"object" 						{return OBJECT_TOKEN;}
"true"							{return TRUE_TOKEN;}
"null"							{return NULL_TOKEN;}
"false"							{return FALSE_TOKEN;}
"print"							{return PRINT;}
"void"							{return VOID;}
"case"							{return CASE;}
"default"						{return DEFAULT;}
"main"							{return MAIN;}
"return"						{return RETURN_TOKEN;}
"switch"						{return SWITCH_TOKEN;}
"break"							{return BREAK_TOKEN;}
"+"								{return OP_PLUS;}
"-"								{return OP_MINUS;}
"*"								{return OP_MULT;}
"/"								{return OP_DIV;}
"%"								{return OP_MOD;}
"<="							{return OP_LE;}
">="							{return OP_GE;}
"=="							{return OP_EQ;}
">"								{return OP_GT;}
"<"								{return OP_LT;}
"="								{return OP_ASSIGN;}
":"								{return COLON;}
"!="							{return OP_NE;}
"||"							{return OP_OR;}
"&&"							{return OP_AND;}
"!"								{return NOT;}
int|char\*    					{yylval.strval=strdup(yytext);return TYPE;}
[a-zA-Z][a-zA-Z0-9]*			{yylval.strval=strdup(yytext);return NAME;}
([-]?[1-9][0-9]*)|0				{yylval.strval=strdup(yytext);return NUMBER;}
"{"								{return OPEN_C_BRACKET;}
"}"								{return CLOSE_C_BRACKET;}
"["								{return OPEN_S_BRACKET;}
"]"								{return CLOSE_S_BRACKET;}
"("								{return OPEN_PARENTHESIS;}
")"								{return CLOSE_PARENTHESIS;}
";"								{return SEMICOLON;}
"."								{return DOT;}
","								{return COMMA;}
[ \t\n]							;
.								{printf("unexpected character\n");}

%%

int yywrap(void)
{
	return 1;
}