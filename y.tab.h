/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     FOR_TOKEN = 258,
     WHILE_TOKEN = 259,
     IF_TOKEN = 260,
     ELSE_TOKEN = 261,
     SWITCH_TOKEN = 262,
     DO_TOKEN = 263,
     OP_PLUS = 264,
     OP_MINUS = 265,
     OP_MULT = 266,
     OP_DIV = 267,
     OP_MOD = 268,
     OP_ASSIGN = 269,
     OP_GT = 270,
     OP_LT = 271,
     OP_GE = 272,
     OP_LE = 273,
     OP_EQ = 274,
     OP_AND = 275,
     OP_OR = 276,
     NOT = 277,
     OP_NE = 278,
     OBJECT_TOKEN = 279,
     TRUE_TOKEN = 280,
     FALSE_TOKEN = 281,
     NULL_TOKEN = 282,
     BREAK_TOKEN = 283,
     OPEN_C_BRACKET = 284,
     CLOSE_C_BRACKET = 285,
     OPEN_S_BRACKET = 286,
     CLOSE_S_BRACKET = 287,
     OPEN_PARENTHESIS = 288,
     CLOSE_PARENTHESIS = 289,
     COLON = 290,
     SEMICOLON = 291,
     COMMA = 292,
     DOT = 293,
     PRINT = 294,
     MAIN = 295,
     VOID = 296,
     CASE = 297,
     DEFAULT = 298,
     END = 299,
     RETURN_TOKEN = 300,
     NAME = 301,
     STRING = 302,
     NUMBER = 303,
     TYPE = 304
   };
#endif
/* Tokens.  */
#define FOR_TOKEN 258
#define WHILE_TOKEN 259
#define IF_TOKEN 260
#define ELSE_TOKEN 261
#define SWITCH_TOKEN 262
#define DO_TOKEN 263
#define OP_PLUS 264
#define OP_MINUS 265
#define OP_MULT 266
#define OP_DIV 267
#define OP_MOD 268
#define OP_ASSIGN 269
#define OP_GT 270
#define OP_LT 271
#define OP_GE 272
#define OP_LE 273
#define OP_EQ 274
#define OP_AND 275
#define OP_OR 276
#define NOT 277
#define OP_NE 278
#define OBJECT_TOKEN 279
#define TRUE_TOKEN 280
#define FALSE_TOKEN 281
#define NULL_TOKEN 282
#define BREAK_TOKEN 283
#define OPEN_C_BRACKET 284
#define CLOSE_C_BRACKET 285
#define OPEN_S_BRACKET 286
#define CLOSE_S_BRACKET 287
#define OPEN_PARENTHESIS 288
#define CLOSE_PARENTHESIS 289
#define COLON 290
#define SEMICOLON 291
#define COMMA 292
#define DOT 293
#define PRINT 294
#define MAIN 295
#define VOID 296
#define CASE 297
#define DEFAULT 298
#define END 299
#define RETURN_TOKEN 300
#define NAME 301
#define STRING 302
#define NUMBER 303
#define TYPE 304




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 54 "roseta.y"
{
	char* strval;
}
/* Line 1529 of yacc.c.  */
#line 151 "y.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

