object Math{

	int termino1;
	int termino2;

	void init(){
		termino1=0;
		termino2=1;
	}

	int fibonacci(int n){
		if(n == 1){
			return termino1;
		}
		if(n == 2){
			return termino2;
		}
		return Math.fibonacci(n - 1) + Math.fibonacci(n - 2);
	}
}


int main(void){
	int numero;
	int resultado;
	numero = -1;
	while(numero <= 0){
		numero = getint("Ingrese un numero entero positivo: ");
	}
	Math.init();
	resultado = Math.fibonacci(numero);
	printf("el termino %d de la sucesion de fibonacci es %d", numero, resultado);
	return 0;
}